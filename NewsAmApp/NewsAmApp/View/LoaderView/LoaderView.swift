//
//  LoaderView.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//
import UIKit
import NVActivityIndicatorView

class LoaderView: UIView {
    @IBOutlet var indicatorView: NVActivityIndicatorView!
	@IBOutlet var indicatorViewWidth: NSLayoutConstraint!
	
    var view: UIView!
	var indicatorType = NVActivityIndicatorType.ballClipRotate
    var loaderColor = UIColor.red
	
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
	
    convenience init(frame: CGRect, width: CGFloat = 35, color: UIColor = .red, indicatorType: NVActivityIndicatorType = .ballClipRotate) {
		self.init(frame: frame)
		indicatorViewWidth.constant = width
		indicatorView.color = color
		indicatorView.type = indicatorType
	}
	
	override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
		super.traitCollectionDidChange(previousTraitCollection)
		
		indicatorView.setNeedsLayout()
	}
    
    private func setup() {
        view = loadViewFromNib(owner: self)
        view.frame = bounds
        
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,
                                 UIView.AutoresizingMask.flexibleHeight]
        
        addSubview(view)
        
		updateStyle()
        indicatorView.padding = 0        
    }
	
	func updateStyle() {
		indicatorView.type = indicatorType
		indicatorView.color = loaderColor
	}
    
	func startAnimating() {
        isHidden = false
        indicatorView.startAnimating()
    }
    
	func stopAnimating() {
        indicatorView.stopAnimating()
        isHidden = true
    }
    
    func loadViewFromNib<T : UIView>(owner: T) -> UIView {
        let bundle = Bundle(for: type(of: owner))
        let nib = UINib(nibName: String(describing: type(of: owner)), bundle: bundle)
        let view = nib.instantiate(withOwner: owner, options: nil)[0] as! UIView
        return view
    }
}
