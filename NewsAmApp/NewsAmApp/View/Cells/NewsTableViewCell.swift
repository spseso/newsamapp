//
//  NewsTableViewCell.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit
import SDWebImage

class NewsTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
        
    func update(withNewsItem newsItem: NewsItem) {
        newsImageView.sd_setImage(with: URL(string: newsItem.imageLinkStr), placeholderImage: UIImage(named: "logo"))
        titleLabel.text = newsItem.title
        dateLabel.text = newsItem.pubDate.getPublicatedString()
        contentView.backgroundColor = newsItem.isNew ? .white : .lightGray
    }
}
