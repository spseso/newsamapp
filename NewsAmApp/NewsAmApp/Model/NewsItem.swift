//
//  NewsItem.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit
import RealmSwift
import AlamofireRSSParser
import SwiftSoup


class NewsItem: Object, NSCopying {
    @objc dynamic var id = ""
    @objc dynamic var title = ""
    @objc dynamic var pubDate = Date()
    @objc dynamic var link = ""
    @objc dynamic var fullText = ""
    @objc dynamic var isViewed = false
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    var isFullTextFetched: Bool {
        return fullText != ""
    }
    
    var isNew: Bool {
        if isViewed {
            return false
        }
        guard let lastDate = DataManager.shared.lastUpdateDate else {
            return true
        }
        return pubDate > lastDate
    }
    
    var imageLinkStr: String {
        get {
            let imageLink = Constants.imageLink(link:id.separate(every: 2, with: "/"))
            return imageLink
        }
    }
    
    convenience init(rssItem: RSSItem) {
        self.init()
        title = rssItem.title ?? title
        pubDate = rssItem.pubDate ?? pubDate
        link = rssItem.link ?? ""
        id = link.extractNewsId()
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let newItem = NewsItem()
        newItem.id = id
        newItem.title = title
        newItem.pubDate = pubDate
        newItem.link = link
        newItem.fullText = fullText
        return newItem
    }
}

extension NewsItem {
    func configureFullText(withHtml html: String) {
        guard let doc: Document = try? SwiftSoup.parse(html) else { return }
        guard let span: Element = try? doc.select("span.article-body").first() else { return }
        guard let attributedText: String = try? span.text() else {return}
        fullText = attributedText
        isViewed = fullText != ""
    }
}
