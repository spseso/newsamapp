//
//  RealmManager.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit
import RealmSwift

protocol ManagerDelegate {
    func dataUpdated()
    func dataUpdateFailed()
}

class RealmManager {
    static let shared = RealmManager()
    
    var delegate: ManagerDelegate?
    
    var realm = try! Realm()
    lazy var news: Results<NewsItem> = { self.realm.objects(NewsItem.self) }()
    
    func update(newsItem: NewsItem) {
        if checkIfNewsItemExist(newsItem: newsItem) {
           removeNewsItem(newsItem: newsItem)
        } else {
            saveNewsItem(newsItem: newsItem)
        }
    }
            
    func saveNewsItem(newsItem: NewsItem) {
        do {
            try realm.write({
                realm.add(newsItem.copy() as! Object)
                self.delegate?.dataUpdated()
            })
        }  catch {
            self.delegate?.dataUpdateFailed()
        }
    }
    
    func removeNewsItem(newsItem: NewsItem) {
        guard let object = realm.object(ofType: NewsItem.self, forPrimaryKey: newsItem.id) else {
            self.delegate?.dataUpdateFailed()
            return
        }
        do {
            try realm.write({
                realm.delete(object)
                self.delegate?.dataUpdated()
            })
        }  catch {
            self.delegate?.dataUpdateFailed()
        }
    }
    
    func checkIfNewsItemExist (newsItem: NewsItem) -> Bool {        
        return realm.object(ofType: NewsItem.self, forPrimaryKey: newsItem.id) != nil
    }
    
}
