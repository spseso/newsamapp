//
//  DataManager.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireRSSParser

class DataManager {
    
    static let shared = DataManager()    
    
    var delegate: ManagerDelegate?
    
    private var dataRequset: DataRequest?    
    
    var lastUpdateDate: Date?
    
    var news: [NewsItem] = []
    
    @objc func updateNews() {
        guard let url = URL(string: Constants.rssApi) else {
            self.delegate?.dataUpdateFailed()
            return
        }
        if let dataRequest = dataRequset, !dataRequest.isFinished {
            dataRequest.cancel()
        }
        dataRequset = AF.request(url).responseRSS { (response) in
            if let feed: RSSFeed = response.value {
                self.updateData(items: feed.items)
                self.delegate?.dataUpdated()
                return
            } else {
                self.delegate?.dataUpdateFailed()
                self.configureNextUpdate()
            }
        }
    }
        
    private func updateData(items: [RSSItem]) {
        defer {
            configureNextUpdate()
        }
        guard news.count != 0 else {
            initializeData(items: items)
            return
        }
        var newInitializedNews: [NewsItem] = []
        for item in items {
            if let localNews = self.news.first {
                if item.link?.extractNewsId() == localNews.id {
                    break
                }
            }
            newInitializedNews.append(NewsItem(rssItem: item))
        }
        if newInitializedNews.count > 0 {
            configureLastUpdateDate()
            news.insert(contentsOf: newInitializedNews, at: 0)
            NotificationManager.fireNotification()
        }
    }
    
    private func initializeData(items: [RSSItem]) {
        for item in items {
            self.news.append(NewsItem(rssItem: item))
        }
    }
        
    func configureLastUpdateDate() {
        if let newsItem = news.first {
            lastUpdateDate = newsItem.pubDate
        }
    }
    
    func configureNextUpdate() {
        let nextUpdateSec = UserDefaults.standard.integer(forKey: Constants.notificationUpdateTimeKey)
        Timer.scheduledTimer(timeInterval: TimeInterval(nextUpdateSec), target: self, selector: #selector(updateNews), userInfo: nil, repeats: false)        
    }    
    
    func fetchNewsFullText(newsItem :NewsItem,completion:@escaping (Bool) -> Void) {
        if newsItem.isFullTextFetched {
            completion(true)
            return
        }
        guard let url = URL(string: newsItem.link) else {
            completion(false)
            return
        }
        AF.request(url).responseString { (response) in
            if let responseString = response.value {
                newsItem.configureFullText(withHtml: responseString)
                completion(true)
                return
            }
            completion(false)
        }
    }        
}
