//
//  NotificationManager.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/22/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit
import UserNotifications

struct NotificationManager {
    
    static func configureDefaultValues() {
        if UserDefaults.standard.integer(forKey: Constants.notificationUpdateTimeKey) == 0 {
            UserDefaults.standard.set(5, forKey: Constants.notificationUpdateTimeKey)
        }
        let notificationCenter = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
    }
    
    static func fireNotification() {
        let content = UNMutableNotificationContent()
        content.title = "Նորությունները թարմացվել են"
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5, repeats: false)
        let request = UNNotificationRequest(identifier: "UpdateNotification", content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error : Error?) in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
}
