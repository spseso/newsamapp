//
//  SettingsViewController.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var secondsPicker: UIPickerView!
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        var row = UserDefaults.standard.integer(forKey: Constants.notificationUpdateTimeKey)
        row = row == 0 ? row : row - 1
        secondsPicker.selectRow(row, inComponent: 0, animated: false)
    }    
}
//MARK: - UIPickerDelegate\DataSource methods
extension SettingsViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 59
    }    

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row + 1) վարկյան"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        UserDefaults.standard.set(row + 5, forKey: Constants.notificationUpdateTimeKey)
    }
}
