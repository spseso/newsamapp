//
//  NewsViewController.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit

//MARK: - BaseViewControllerProtocol methods
class NewsViewController: BaseNewsViewController {
    override func fetchData() {
        super.fetchData()
        DataManager.shared.updateNews()
        DataManager.shared.delegate = self
    }
    
    override func newsCount() -> Int {
        return DataManager.shared.news.count
    }

    override func newsItem(forIndexPath indexPath: IndexPath) -> NewsItem {
        return DataManager.shared.news[indexPath.row]
    }
}
