//
//  NewsInnerViewController.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit

class NewsInnerViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fullTextLabel: UILabel!
    
    //MARK: - Public Variables
    var newsItem: NewsItem!
    
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        configureNavigationBar()        
    }
    
    //MARK: - Private methods
    private func updateUI() {
        newsImageView.sd_setImage(with: URL(string: newsItem.imageLinkStr), placeholderImage: UIImage(named: "logo"))
        titleLabel.text = newsItem.title
        dateLabel.text = newsItem.pubDate.getPublicatedString()
        fullTextLabel.text = newsItem.fullText
    }
    
    private func configureNavigationBar() {
        let imName = RealmManager.shared.checkIfNewsItemExist(newsItem: newsItem) ? "trash" : "tray.and.arrow.down"
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: imName), style: .plain, target: self, action: #selector(rightBarButtonAction(_:)))        
    }

    @objc private func rightBarButtonAction(_ sender: Any) {
        UIApplication.showLoader()
        RealmManager.shared.update(newsItem: newsItem)
        UIApplication.hideLoader()
        configureNavigationBar()
    }
}
