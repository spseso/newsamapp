//
//  BaseNewsViewController.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit

protocol BaseViewControllerProtocol {
    func newsCount() -> Int
    func newsItem(forIndexPath: IndexPath) -> NewsItem
    func fetchData()
}

class BaseNewsViewController: UIViewController, BaseViewControllerProtocol, ManagerDelegate {
    //MARK: - IBOutlets
    @IBOutlet weak var newsTableView: UITableView!
    
    //MARK: - Private Variables
    private var refreshControl = UIRefreshControl()
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        fetchData()
    }
    
    //MARK: - TableView Related methods
    private func configureTableView() {
        newsTableView.tableFooterView = UIView()
        newsTableView.register(UINib(nibName: String(describing: NewsTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: NewsTableViewCell.self))
        configureRefreshControl()
    }
    
    private func configureRefreshControl() {
        refreshControl.tintColor = .red
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        newsTableView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
       fetchData()
    }
    
    //MARK: - Public Methods
    func scrollToTop() {
        if newsCount() > 0 {
            newsTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
    }
    
    
    //MARK: - Private Methods
    private func pushInner(withNewsItem newsItem: NewsItem) {
        let newsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: NewsInnerViewController.self)) as! NewsInnerViewController
        newsVC.newsItem = newsItem
        navigationController?.pushViewController(newsVC, animated: true)
    }
    
    //MARK: - BaseViewControllerProtocol methods, must be overriden by subclasses
    func newsItem(forIndexPath indexPath: IndexPath) -> NewsItem {
        assert(false)
        return NewsItem()
    }

    func newsCount() -> Int {
        assert(false)
        return 0
    }
    
    func fetchData() {
        if !refreshControl.isRefreshing {
            UIApplication.showLoader()
        }
    }
    
    //MARK:- Manager Delegate methods
    func dataUpdated() {
        refreshControl.endRefreshing()
        UIApplication.hideLoader()
        newsTableView.reloadData()
    }
    
    func dataUpdateFailed() {
        refreshControl.endRefreshing()
        UIApplication.hideLoader()
        showAlert(title: "Սխալ", message: "Ստուգել կապը")
    }
    
}

//MARK: - TableView Delegate/DataSource methods

extension BaseNewsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsTableViewCell.self)) as! NewsTableViewCell
        let item = newsItem(forIndexPath: indexPath)
        cell.update(withNewsItem: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = newsItem(forIndexPath: indexPath)
        UIApplication.showLoader()
        DataManager.shared.fetchNewsFullText(newsItem: item) { [weak self] (isSuccess) in
            guard let self = self else {return}
            UIApplication.hideLoader()
            if isSuccess {
                self.newsTableView.reloadRows(at: [indexPath], with: .none)
                self.pushInner(withNewsItem: item)
            } else {
                self.showAlert(title: "Սխալ", message: "Ստուգել կապը")
            }
        }
    }      
    
}
