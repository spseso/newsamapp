//
//  ArchiveViewController.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit

//MARK: - BaseViewControllerProtocol methods
class ArchiveViewController: BaseNewsViewController {
    override func fetchData() {
        super.fetchData()
        RealmManager.shared.delegate = self
        dataUpdated()
        
    }
    
    override func newsCount() -> Int {
        return RealmManager.shared.news.count
    }
    
    override func newsItem(forIndexPath indexPath: IndexPath) -> NewsItem {
        return RealmManager.shared.news[indexPath.row].copy() as! NewsItem
    }
}

//MARK: - TableView Delegate/DataSource methods
extension ArchiveViewController {
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .insert
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
         return "Ջնջել"
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            UIApplication.showLoader()
            RealmManager.shared.update(newsItem: newsItem(forIndexPath: indexPath))
            UIApplication.hideLoader()
        }
    }
}
