//
//  Contants.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit

struct Constants {
    static let rssApi = "https://news.am/arm/rss/"
    
    static func imageLink(link: String) -> String {
        return "https://news.am/img/news/\(link)/default.jpg"
    }
    
    static let notificationUpdateTimeKey = "NotificationTimeKey"
}
