//
//  UIApplicationExtensions.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit
import PureLayout

extension UIApplication {
    static var loaderView: LoaderView!

    static func showLoader() {
        guard let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first else { return }
        if nil == UIApplication.loaderView {
            UIApplication.loaderView = LoaderView(frame: window.bounds)
        }
        if loaderView.superview != nil {
            UIApplication.loaderView.removeFromSuperview()
        }
        window.addSubview(UIApplication.loaderView)
        UIApplication.loaderView.autoPinEdgesToSuperviewEdges()
        UIApplication.loaderView.startAnimating()
    }
    
    static func hideLoader() {
        if UIApplication.loaderView != nil {
            UIApplication.loaderView.stopAnimating()
            UIApplication.loaderView.removeFromSuperview()
        }
    }
}
