//
//  StringExtensions.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit

extension String {
    func separate(every stride: Int = 4, with separator: Character = " ") -> String {
        return String(enumerated().map { $0 > 0 && $0 % stride == 0 ? [separator, $1] : [$1]}.joined())
    }
    
    func extractNewsId() -> String {
        return (self as NSString).lastPathComponent.replacingOccurrences(of: ".html", with: "")
    }
}
