//
//  DateExtensions.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import Foundation

extension Date {
    func getPublicatedString() -> String? {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "hy")
        formatter.dateFormat = "MMMM dd, HH:mm"
        return formatter.string(from: self)
    }
}
