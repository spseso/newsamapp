//
//  UIViewControllerExtensions.swift
//  NewsAmApp
//
//  Created by Sevak Soghoyan on 6/23/20.
//  Copyright © 2020 Sevak Soghoyan. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String, actions: [UIAlertAction] = [], completion: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if actions.isEmpty {
            alertController.addAction(UIAlertAction(title: "Լավ", style: .cancel))
        } else {
            for action in actions {
                alertController.addAction(action)
            }
        }
        
        present(alertController, animated: true, completion: completion)
    }
}
